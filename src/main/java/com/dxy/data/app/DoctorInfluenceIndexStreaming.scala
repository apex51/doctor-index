package com.dxy.data.app

import com.dxy.data.util.{ConfigUtil, DoctorIndexDao, StreamingUtil}
import com.dxy.data.util.StreamingUtil
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext, TestInputStream}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by jianghao on 17/2/28.
  */
object DoctorInfluenceIndexStreaming {

  def main(args: Array[String]): Unit = {

    val debug = false
    val local = false

    val days = ConfigUtil.days
    var newArgs = args
    if (debug) {
      newArgs = Array("", "", "10")
    }

    if (newArgs.length != 3) {
      System.err.println("USAGE:")
      System.err.println(
        "< kafka_topic > < offset_path > < interval_seconds >")
      System.exit(1)
    }

    // for doctor index
    val userIDTopicName = newArgs(0)
    val userIDOffsetPath = newArgs(1)
    val userIDIntervalSeconds = newArgs(2).toLong
    val kafkaParams = Map("metadata.broker.list" -> ConfigUtil.kafkaBrokerList)

    // Init sparkContext, streamingContext, hiveContext.
    val sparkConf =
      new SparkConf().setAppName("PI-DoctorInfluenceIndexStreaming")

    if (local) sparkConf.setMaster("local[2]")

    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("WARN")
    val ssc = new StreamingContext(sc, Seconds(userIDIntervalSeconds))
    val hdfs = FileSystem.get(sc.hadoopConfiguration)
    val streamingUtil = new StreamingUtil(ssc, hdfs)
    val sqlContext = new SQLContext(sc)

    /**
      * ========================================
      * Stream 1:     kafka streams
      * ========================================
      */
    val UserKeyWordsStream: DStream[(String, String)] = if (debug) {
      new TestInputStream[String](
        ssc,
        Seq(Seq("""{"document_id":"bbs_thread|21909"}""")),
        2).map(f => ("", f))
    } else
      streamingUtil
        .createDStream(Set(userIDTopicName), kafkaParams, userIDOffsetPath)

    // process 1 ,get user's keywords to es
    UserKeyWordsStream.map(_._2).foreachRDD { rdd =>
      // FUNC 0 ===> Mapping Log
      // If empty rdd is not ruled out, 'select' will meet exceptions.
      val logRDD = if (rdd.isEmpty) {
        rdd.sparkContext.emptyRDD[String] // Nothing will happen here.
      } else {
        sqlContext.read
          .json(rdd)
          .select("document_id")
          .map { case Row(dxyid: String) => dxyid }
          .distinct
      }

      // ========
      streamingUtil.printCount("logRDD(num of documents)", logRDD.cache)

      logRDD.repartition(4).foreachPartition { iterator =>
        iterator.foreach(documentId => {
          DoctorIndexDao.updateDoctorInfluenceToday(documentId)
        })
      }

      // =========
      if (debug) {
        println("\n\n")
        streamingUtil.printCount("DoctorInfluenceIndexLogRDD", logRDD)
      }
    }

    // Checkpoint offset to file.
    if (!debug)
      streamingUtil.saveOffset(UserKeyWordsStream, userIDOffsetPath)

    ssc.start
    ssc.awaitTermination
  }

}
