package com.dxy.data.app

import com.dxy.data.util.{ConfigUtil, StreamingUtil, UserInformationToES}
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.hive.ql.exec.Utilities.SQLCommand
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext, TestInputStream}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by jianghao on 17/2/15.
  */
object UserKeyWordsStreaming {

  def main(args: Array[String]): Unit = {

    val debug = false
    val local = false

    var newArgs = args
    if (debug) {
      newArgs = Array("", "", "10", "", "")
    }
    if (newArgs.length != 5) {
      System.err.println("USAGE:")
      System.err.println(
        "<kafka topic> <offset path> <interval seconds> <index name>" +
          " <ES url, use ip:port like 10.25.15.129:9200>")
      System.exit(1)
    }
    // for userKeywords
    val userIDTopic = newArgs(0)
    val keywordsStreamOffsetPath = newArgs(1)
    val userIDIntervalSeconds = newArgs(2).toLong
    val userIDKeywordsIndexName = newArgs(3)
    val userIDKeywordsESUrl = newArgs(4)

    val kafkaParams = Map("metadata.broker.list" -> ConfigUtil.kafkaBrokerList)

    // Init sparkContext, streamingContext, hiveContext.
    val sparkConf = new SparkConf().setAppName("PI-UserKeywordsCloudStreaming")

    if (local) sparkConf.setMaster("local[2]")

    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("WARN")
    val ssc = new StreamingContext(sc, Seconds(userIDIntervalSeconds))
    val hdfs = FileSystem.get(sc.hadoopConfiguration)
    val streamingUtil = new StreamingUtil(ssc, hdfs)
    val sqlContext = new SQLContext(sc)


    /**
      * ========================================
      * Stream 1:     kafka streams
      * ========================================
      */
    val UserKeyWordsStream: DStream[(String, String)] =
      if (debug) {
        new TestInputStream[String](
          ssc,
          Seq(Seq("""{"dxyid":"tommyxxx","cookie_id":"","mc":""}""")),
          2).map(f => ("", f))
      } else
        streamingUtil.createDStream(Set(userIDTopic),
                                    kafkaParams,
                                    keywordsStreamOffsetPath)

    // get user's keywords to es
    UserKeyWordsStream.map(_._2).foreachRDD { rdd =>
      // FUNC 0 ===> Mapping Log
      // If empty rdd is not ruled out, 'select' will meet exceptions.
      val logRDD = if (rdd.isEmpty) {
        rdd.sparkContext.emptyRDD[String] // Nothing will happen here.
      } else {
        sqlContext.read
          .json(rdd)
          .select("dxyid")
          .map { case Row(dxyid: String) => dxyid }
          .distinct
      }

      // ========
      streamingUtil.printCount("logRDD(num of users)", logRDD.cache)

      logRDD.repartition(8).foreachPartition { iterator =>
        iterator.foreach(
          dxyid =>
            // Get user's keywords to ES.
            UserInformationToES.userKeywordsToES(dxyid, userIDKeywordsIndexName, userIDKeywordsESUrl))
      }

      // =========
      if (debug) {
        println("\n\n")
        streamingUtil.printCount("keyWordsLogRDD", logRDD)
      }
    }

    // Checkpoint offset to file.
    if (!debug)
      streamingUtil.saveOffset(UserKeyWordsStream, keywordsStreamOffsetPath)

    ssc.start
    ssc.awaitTermination
  }
}
