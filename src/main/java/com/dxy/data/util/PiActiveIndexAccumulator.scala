package com.dxy.data.util

import java.util.Date

import com.dxy.data.hbase.daos.{PiUserInfoDao}
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.hadoop.hbase.CellUtil
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json

/**
  * Created by jianghao on 17/2/21.
  */
object PiActiveIndexAccumulator {

  val logger: Logger = LoggerFactory.getLogger(PiActiveIndexAccumulator.getClass)

  val piUserInfoDao = new PiUserInfoDao()

  /**
    * 根据 user id 获得某一天的所有行为
    *
    * @param userId
    * @param day
    * @return
    */
  def getDoctorActionListByUserID(userId: String, day: Date): List[(String, String)] = {
    val tagList = piUserInfoDao.findTagsByUserId(userId)
    tagList.flatMap { tag =>
      getDoctorActionListByTag(tag._1, tag._2, day)
    }
  }

  /**
    * 根据 user tag 获得某一天的所有行为
    *
    * @param tag
    * @param day
    * @return
    */
  def getDoctorActionListByTag(tagType: String, tag: String, day: Date): List[(String, String)] = {
    val userTagRowkey = HBaseContent.piUserLogRowkey(tagType, tag, day)
    val result = HBaseUtil.get(HBaseContent.piUserLogTableName, userTagRowkey)
    result.rawCells.map(cell => {
      try {
        val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
        (((valueJson \ "account").asOpt[String].getOrElse(""),
          (valueJson \ "action").asOpt[String].getOrElse("")))
      } catch {
        case e: Exception =>
          logger.error(e.getMessage, e)
          ("", "")
      }
    }).filter(_._1.nonEmpty).toList
  }

  /**
    * 各个行为对应的活跃度次数
    */
  def getDoctorIndexActivity(actionList: List[(String, String)]): Map[String, Int] = {
    val list = actionList.filter { case (account, action) => account.nonEmpty && action.nonEmpty }

    if (list.isEmpty) {
      logger.error("no action")
      Map()
    } else {
      // 各个行为对应的总次数
      val actionTimesMap = list.groupBy(f => f).map {
        case ((account: String, action: String), groupList: List[(String, String)]) =>
          ((account, action), groupList.size)
      }

      // 计算各个行为对应的总分
      actionTimesMap.map { case ((account: String, action: String), count: Int) =>
        ConfigUtil.doctorIndexConfig.get(account) match {
          case Some(accountEntry) =>
            accountEntry.get(action) match {
              case Some(actionEntry) => (actionEntry.getOrElse("action", "").toString, count)
              case _ => ("", 0)
            }
          case _ => ("", 0)
        }
      }.filter(_._1.nonEmpty)
    }
  }

  /**
    * activity index list by userTag
    */
  def getDoctorIndexMapByUserTag(tagType: String, tag: String, day: Date): Map[String, Int] = {
    val actionList = getDoctorActionListByTag(tagType, tag, day)
    getDoctorIndexActivity(actionList)
  }

  /**
    * activity index list by userId
    */
  def getDoctorIndexMapByUserId(userId: String, day: Date): Map[String, Int] = {
    val actionList = getDoctorActionListByUserID(userId, day)
    getDoctorIndexActivity(actionList)
  }

//  /**
//    * 统计累加各个行为对应的活跃度
//    *
//    * @param actionList : the user's actionList
//    * @return
//    */
//  def getDocterIndex(actionList: List[(String, String)]): Map[String, PiDoctorIndex] = {
//    val list = actionList.filter { case (account, action) => account.nonEmpty && action.nonEmpty }
//
//    val timesMap = mutable.Map[String, PiDoctorIndex]()
//    if (list.isEmpty) {
//      logger.error("no action")
//    } else {
//      // 各个行为对应的总次数
//      val actionTimesMap = list
//        .groupBy(f => f)
//        .map { case ((account: String, action: String), groupList: List[(String, String)]) =>
//          ((account, action), groupList.size)
//        }
//
//      // 计算各个行为对应的总分
//      actionTimesMap.foreach { case ((account: String, action: String), count: Int) =>
//
//        ConfigUtil.doctorIndexConfig.get(account).foreach { accountEnrtry => accountEnrtry.get(action).foreach {
//          actionEntry =>
//
//            timesMap.put(account + "#" + action,
//              PiDoctorIndex(
//                String.valueOf(actionEntry.getOrElse("action", "error")),
//                count * actionEntry.getOrElse("activity_score", 0).toString.toInt,
//                count * actionEntry.getOrElse("professional_score", 0).toString.toInt,
//                count * actionEntry.getOrElse("influence_score", 0).toString.toInt,
//                count))
//        }
//        }
//      }
//
//      /*val num = ConfigUtil.num
//      actionTimesMap.foreach { case ((account: String, action: String), count: Int) =>
//
//        ConfigUtil.doctorIndexConfig.get(account).foreach { accountEnrtry => accountEnrtry.get(action).foreach {
//          actionEntry =>
//
//            timesMap.put(account + "#" + action,
//              PiDoctorIndex(
//                String.valueOf(actionEntry.getOrElse("action", "error")),
//                (2 * Math.pow(1+ Math.pow(Math.E,- count * actionEntry.getOrElse("activity_score", 0).toString.toInt),-1) -1).toInt * num,
//                (2 * Math.pow(1+ Math.pow(Math.E,- count * actionEntry.getOrElse("professional_score", 0).toString.toInt),-1) -1).toInt * num,
//                (2 * Math.pow(1+ Math.pow(Math.E,- count * actionEntry.getOrElse("influence_score", 0).toString.toInt),-1) -1).toInt * num,
//                count))
//        }
//        }
//      }*/
//    }
//    timesMap.toMap
//  }
//
//
//  /**
//    * 得到一个 user Tag 的 某一天的指数
//    *
//    * @param tag
//    * @param day
//    * @return
//    */
//  def getDoctorIndexMapByUserTag(tagType: String, tag: String, day: Date): (String, Map[String, PiDoctorIndex]) = {
//    val actionList = getDoctorActionListByTag(tagType, tag, day)
//    val actionTimes = getDocterIndex(actionList)
//    (tag, actionTimes)
//  }
//
//  /**
//    * 得到一个 userID 的 某一天的指数
//    *
//    * @param userId
//    * @param day
//    * @return
//    */
//  def getDoctorIndexMapByUserId(userId: String, day: Date): (String, Map[String, PiDoctorIndex]) = {
//    val actionList = getDoctorActionListByUserID(userId, day)
//    val actionTimes = getDocterIndex(actionList)
//    (userId, actionTimes)
//  }

}
