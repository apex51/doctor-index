package com.dxy.data.util

import java.util.Date

import com.dxy.data.hbase.daos.PiDoctorIndexDao

/**
  * Created by jianghao on 17/2/22.
  */
object DoctorIndexDao {

  val piDoctorIndexDao = new PiDoctorIndexDao()

  // update doctor activity today
  def updateDoctorActivityToday(userId: String): Unit = {
    val date = new Date()
    updateDoctorActivity(userId, date)
  }

  // update doctor activity in a specific day
  def updateDoctorActivity(userId: String, date: Date): Unit = {
    val doctorActivityMap = PiActiveIndexAccumulator.getDoctorIndexMapByUserId(userId, date)
    if (doctorActivityMap.nonEmpty) {
      piDoctorIndexDao.putDoctorActivityDay(userId, date, doctorActivityMap)
    }
  }

  // update doctor influence today
  def updateDoctorInfluenceToday(documentId: String) = {
    val date = new Date()
    updateDoctorInfluence(documentId, date)
  }

  // update doctor influence in a specific day
  def updateDoctorInfluence(documentId: String, date: Date) = {
    val doctorInfluenceMap = PiInfluenceIndexAccumulator.getDoctorInfluenceIndexMapByDocumentId(documentId, date)
    if (doctorInfluenceMap._1.nonEmpty && doctorInfluenceMap._2.nonEmpty) {
      piDoctorIndexDao.putDoctorInfluenceDay(doctorInfluenceMap._1, documentId, date, doctorInfluenceMap._2)
    }
  }

//  // update doctor influence today
//  def updateDoctorInfluenceIndexToday(userId: String, days: Int): Unit = {
//    val date = new Date()
//
//    val res = HBaseUtil.get(HBaseContent.piDoctorIndexTableName,
//                            HBaseContent.piDoctorIndexRowkey(userId, date),
//                            "f",
//                            HBaseContent.piDoctorIndexColumn_influenceTime)
//
//    val cells: Array[Cell] = res.rawCells
//    if (cells.size == 1) {
//      val timestamp = new String(CellUtil.cloneValue(cells.head))
//      if (System.currentTimeMillis() - timestamp.toLong < 1000 * 60 * 60) {
//        return
//      }
//    }
//
//    val doctorInfluenceIndexMap = new PiInfluenceIndexAccumulator()
//      .getDoctorInfluenceIndexMapByUserId(userId, days, date)
//    new PiDoctorIndexDao()
//      .putDoctorIndexDay(userId, date, doctorInfluenceIndexMap)
//  }
//
//  // update doctor influence in a given day
//  def updateDoctorInfluenceIndexOneDay(userId: String, days: Int, date: Date): Unit = {
//    // to be continued
//  }
}
