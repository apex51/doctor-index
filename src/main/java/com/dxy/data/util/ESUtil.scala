package com.dxy.data.util

import java.net.{URI, URLEncoder}

import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.json.JSONObject

/**
  * Created by jianghao on 17/2/14.
  */
object ESUtil {

  /**
    *
    * 把用户与其相应的关键词放入到 es 中
    * @param indexName: in es ,like the table name
    * @param userID
    * @param content
    * @param esURL: accept url only. eg : 10.25.15.129:9200
    * @return
    */
  def bulkData(indexName: String,
               userID: String,
               content: String,
               esURL: String) =
    try {
      val obj = new JSONObject()
      obj.put("content", content)
      val id = URLEncoder.encode(userID, "utf-8")
      Request
        .Put(new URI("http://" + esURL + "/" + indexName + "/dxy/" + id))
        .connectTimeout(2000)
        .socketTimeout(2000)
        .bodyString(obj.toString, ContentType.APPLICATION_JSON)
        .execute()
        .returnContent()
        .asString()
    } catch {
      case e: Exception => e.printStackTrace(); System.exit(1)
    }
}
