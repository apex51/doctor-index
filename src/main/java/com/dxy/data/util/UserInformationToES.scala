package com.dxy.data.util

import com.dxy.data.hbase.service.PiUserIdToCertainTop

import scala.collection.mutable

/**
  * Created by jianghao on 17/2/15.
  */
object UserInformationToES {

  /**
    *  对于一个用户,获得他的指定时间内的关键词排序列表
    * @param userId
    * @param indexName: in es ,like the table name
    * @param esURL: accept url only. eg : 10.25.15.129:9200
    */
 def userKeywordsToES(userId : String, indexName : String, esURL: String): Unit ={

    val sortedKeyWordList = new PiUserIdToCertainTop().piUserIdToCertainTop(userId)
    val s = new mutable.StringBuilder()
    sortedKeyWordList.foreach(x =>{
      val number = x._2
      for (i <-  1 to number ){
        s.append(x._1)
        s.append(",")
      }
    })
   val content = s.toString()

    // put data to es
    ESUtil.bulkData(indexName, userId, content, esURL)

  }



}
