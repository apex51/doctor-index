package com.dxy.data.util

import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.SafeConstructor

import scala.io.Source

/**
  * Created by jianghao on 17/2/15.
  */
object ConfigUtil {

  val kafkaBrokerList =
    "kafka1.lxc.host.dxy:9092,kafka2.lxc.host.dxy:9092,kafka3.lxc.host.dxy:9092"

  /**
    * 医生指数相关配置
    */
  val doctorIndexConfig = loadYamlMapMap("/doctorIndex.yaml")

  // 这里是用在 (  2 /(1+ e (-x))-1 ) * num
  val num = 15

  // 用于计算医生的影响力指数,获取用户多少天内的文章,在这里暂时设定为一年
  val days = 365

  /**
    * read index from yaml file:
    * using, load
    */
  // convenient func to handle external files, using loan pattern
  def using[A](r: Source)(f: Source => A): A =
  try f(r)
  finally r.close()

  // load map-map from yaml
  def loadYamlMapMap(yamlPath: String) = {
    val yaml = new Yaml(new SafeConstructor)
    val source = using(Source.fromURL(getClass.getResource(yamlPath))) { _.mkString }

    import java.util.{Map => JMap}

    import scala.collection.JavaConverters._

    val javaConfig = yaml
      .load(source)
      .asInstanceOf[JMap[String, JMap[String, JMap[String, Any]]]]
      .asScala
      .toMap
      .mapValues(_.asScala.toMap.mapValues(_.asScala.toMap))
    javaConfig
  }

}
