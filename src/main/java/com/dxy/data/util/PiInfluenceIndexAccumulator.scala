package com.dxy.data.util

import java.util.Date

import com.dxy.data.hbase.daos.PiDocumentUserDao
import com.dxy.data.hbase.models.{PiDocumentUser, PiUserContent}
import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
import org.apache.hadoop.hbase.CellUtil
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json

import scala.collection.mutable

/**
  * Created by jianghao on 17/2/27.
  */
object PiInfluenceIndexAccumulator {

  val logger: Logger = LoggerFactory.getLogger(PiInfluenceIndexAccumulator.getClass)

  val piDocumentUserDao = new PiDocumentUserDao()

  /**
    * 根据 documentID，获得该内容相关的行为影响力
    */
  def getDoctorInfluenceIndexMapByDocumentId(documentId: String, date: Date): (String, Map[String, Int]) = {
    // 根据文章 id 获得用户名
    piDocumentUserDao.getByDocumentId(documentId) match {
      case Some(piDocumentUser) =>
        // 计算内容对应的所有 action 活跃度，作为该内容的影响力
        val userActionList = getActionListByDocumentId(piDocumentUser, date)
        (piDocumentUser.userId, computeActiveScoreByActionList(userActionList))
      case _ => ("", Map())
    }
  }

  /**
    * 获得所有用户对 documentID 内容的 account_action_List
    */
  def getActionListByDocumentId(piDocumentUser: PiDocumentUser,
                                date: Date): List[(String, String)] = {
    val startRow = HBaseContent.piDocumentUserActionRowkeyPrefixWithDate(piDocumentUser.documentId, date)
    val endRow = startRow + "~"
    val results = HBaseUtil.scanData(HBaseContent.piDocumentUserActionTableName, null, startRow, endRow)

    results.flatMap(result => {
      result.rawCells.map(cell => {
        try {
          val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
          (((valueJson \ "account").asOpt[String].getOrElse(""),
            (valueJson \ "action").asOpt[String].getOrElse("")))
        } catch {
          case e: Exception =>
            logger.error(e.getMessage, e)
            (("", ""))
        }
      })
    })
  }


  /**
    * 计算每种 action 活跃度
    */
  def computeActiveScoreByActionList(actionList: List[(String, String)]): Map[String, Int] = {
    val list = actionList.filter {
      case (account, action) => account.nonEmpty && action.nonEmpty
    }

    if (list.isEmpty) {
      logger.error("no action")
      Map()
    } else {
      // 各个行为对应的总次数
      val actionTimesMap = list.groupBy(f => f).map {
        case ((account: String, action: String), groupList: List[(String, String)]) =>
          ((account, action), groupList.size)
      }

      // 计算各个行为对应的总分
      actionTimesMap.map { case ((account: String, action: String), count: Int) =>
        ConfigUtil.doctorIndexConfig.get(account) match {
          case Some(accountEntry) =>
            accountEntry.get(action) match {
              case Some(actionEntry) => (actionEntry.getOrElse("action", "").toString, count)
              case _ => ("", 0)
            }
          case _ => ("", 0)
        }
      }.filter(_._1.nonEmpty)
    }
  }


  //
  //  /**
  //    * 获得 用户 最近 n 天的内容 在 当天产生的 actionList
  //    *
  //    * @param userID
  //    * @param days : 用户最近 n 天的内容
  //    * @param date : 时间
  //    * @return
  //    */
  //  def getActionListFromUserDocument(userID: String,
  //                                    days: Int,
  //                                    date: Date): List[(String, String)] = {
  //
  //    val piUserContentDao = new PiUserContentDao()
  //    val piUserContentList = piUserContentDao.findContentsByUserId(userID, days)
  //    val accountActionList = new mutable.MutableList[(String, String)]()
  //
  //    piUserContentList.foreach(piUserContent => {
  //      accountActionList ++= getActionListByDocumentId(piUserContent, date)
  //    })
  //    accountActionList.toList
  //  }
  //
  //  // 对一个 documentID ,获得其下所有 用户对这篇文章的 account_action_List ( List[(String, String)] )
  //  def getActionListByDocumentId(piUserContent: PiUserContent,
  //                                date: Date): List[(String, String)] = {
  //
  //    val startRow = HBaseContent
  //      .piDocumentUserActionRowkeyPrefixWithDate(piUserContent.documentId, date)
  //    val endRow = startRow + "~"
  //    val results = HBaseUtil.scanData(
  //      HBaseContent.piDocumentUserActionTableName,
  //      null,
  //      startRow,
  //      endRow)
  //    val accountActionList = new mutable.MutableList[(String, String)]()
  //
  //    results.foreach(result => {
  //      result.rawCells.foreach(cell => {
  //        try {
  //          val valueJson =
  //            Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(cell)))
  //          accountActionList += (((valueJson \ "account")
  //            .asOpt[String]
  //            .getOrElse(""),
  //            (valueJson \ "action")
  //              .asOpt[String]
  //              .getOrElse("")))
  //        } catch {
  //          case e: Exception =>
  //            logger.error(e.getMessage, e)
  //        }
  //      })
  //    })
  //
  //    accountActionList.toList
  //
  //  }
  //
  //  /**
  //    *
  //    * @param actionList
  //    * @return
  //    */
  //  def computeActiveScoreByActionList(actionList: List[(String, String)]): Map[String, PiDoctorIndex] = {
  //
  //    val list = actionList.filter {
  //      case (account, action) => account.nonEmpty && action.nonEmpty
  //    }
  //    val timesMap = mutable.Map[String, PiDoctorIndex]()
  //    if (list.isEmpty) {
  //      logger.error("no action")
  //    } else {
  //      // 各个行为对应的总次数
  //      val actionTimesMap = list.groupBy(f => f).map {
  //        case ((account: String, action: String),
  //        groupList: List[(String, String)]) =>
  //          ((account, action), groupList.size)
  //      }
  //      // 计算各个行为对应的总分
  //      actionTimesMap.foreach {
  //        case ((account: String, action: String), count: Int) =>
  //          ConfigUtil.doctorIndexConfig.get(account).foreach { accountEntry =>
  //            accountEntry.get(action).foreach { actionEntry =>
  //              timesMap.put(
  //                account + "#" + action,
  //                PiDoctorIndex(
  //                  String.valueOf(actionEntry.getOrElse("action", "error")),
  //                  0,
  //                  0,
  //                  count * actionEntry
  //                    .getOrElse("activity_score", 0)
  //                    .toString
  //                    .toInt,
  //                  count))
  //            }
  //          }
  //      }
  //    }
  //    timesMap.toMap
  //  }
  //
  //  /**
  //    * 返回值：
  //    * Map[String,Int], String 为account
  //    */
  //  def mergeActiveScore(activeScores: Map[String, PiDoctorIndex]): Map[String, PiDoctorIndex] = {
  //
  //    val result = new mutable.HashMap[String, PiDoctorIndex]
  //    activeScores.toSeq
  //      .map(activeScore => (activeScore._1.split("#")(0), activeScore._2)).foreach{ f =>
  //      if (result.contains(f._1)) {
  //        result(f._1) = result(f._1).copy(
  //          action = f._1,
  //          actionName = f._1,
  //          activity_score = result(f._1).activity_score + f._2.activity_score,
  //          influence_score = result(f._1).influence_score + f._2.influence_score,
  //          professional_score = result(f._1).professional_score + f._2.professional_score
  //        )
  //      } else {
  //        result(f._1) = f._2
  //      }
  //    }
  //
  //    result.toMap
  //
  //  }
  //
  //  // 得到一个个用户所有文章,在当天 引起的影响力
  //  def getDoctorInfluenceIndexMapByUserId(userId: String,
  //                                         days: Int,
  //                                         date: Date): Map[String, PiDoctorIndex] = {
  //
  //    // 获得一个用户的所有的 (account,action) 列表
  //    val accountActionList = getActionListFromUserDocument(userId, days, date)
  //
  //    // 获得所有的 accountActionScore
  //    val accountActionScore = computeActiveScoreByActionList(accountActionList)
  //
  //    // 得到对应于 account 区域的影响力
  //    mergeActiveScore(accountActionScore)
  //  }
}
