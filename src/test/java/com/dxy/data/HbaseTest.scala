//package com.dxy.data
//
//import java.util.Date
//
//import com.dxy.data.hbase.{HBaseContent, HBaseUtil}
//import com.dxy.data.util.{DoctorIndexDao, PiActiveIndexAccumulator}
//import org.apache.hadoop.hbase.{CellUtil, TableName}
//import play.api.libs.json.Json
//
//
//class HbaseTest extends BaseTest {
//
//  val userId = "tommyxxx"
//  val day = new Date()
//  val tableNames = Array("pi_user_info", "pi_user_log", "pi_doctor_index","pi_document_user_action","pi_user_content")
//  val F = "f"
//
//
//  override def beforeEach() {
//    tableNames.foreach { tableName =>
//      if (!HBaseUtil.conn.getAdmin.tableExists(TableName.valueOf(tableName))) {
//        HBaseUtil.createTable(tableName, Array("f", "i"))
//      }
//
//    }
//    HBaseUtil.put(HBaseContent.piUserInfoTableName, HBaseContent.piUserInfoRowkey(userId, HBaseContent.TAG_TYPE_U), F, "q", userId)
//    HBaseUtil.put(HBaseContent.piUserLogTableName, HBaseContent.piUserLogRowkey(HBaseContent.TAG_TYPE_U, HBaseContent.stringToMD5(userId), day), F, "28360592",s"""{"atime": ${System.currentTimeMillis()}, "action": "event_share", "document_id": "bbs_thread|123", "content": "", "account": "bbs"}""")
//
//    HBaseUtil.put(HBaseContent.piUserContentTableName,HBaseContent.piUserContentRowkey(userId,day),F,"1","{\"atime\":1372991777000,\"document_id\":\"bbs_thread|25981720\"}")
//    HBaseUtil.put(HBaseContent.piUserContentTableName,HBaseContent.piUserContentRowkey(userId,day),F,"2","{\"atime\":1064892697000,\"document_id\":\"bbs_thread|409985\"}")
//    HBaseUtil.put(HBaseContent.piUserContentTableName,HBaseContent.piUserContentRowkey(userId,day),F,"3","{\"atime\":1064892697002,\"document_id\":\"bbs_thread|409986\"}")
//    HBaseUtil.put(HBaseContent.piUserContentTableName,HBaseContent.piUserContentRowkey(userId,day),F,"4","{\"atime\":1064892697001,\"document_id\":\"bbs_thread|7777\"}")
//    HBaseUtil.put(HBaseContent.piUserContentTableName,HBaseContent.piUserContentRowkey(userId,day),F,"5","{\"atime\":1064892697001,\"document_id\":\"bbs_thread|7778\"}")
//
//    HBaseUtil.put(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|25981720",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId)),
//      F,"65478000","{\"atime\":1182334278000,\"action\":\"show_post-topic\",\"document_id\":\"bbs_thread|25981720\",\"content\":\"\",\"account\":\"10002-4\"}")
//
//    HBaseUtil.put(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|409985",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId)),
//      F,"65472000","{\"atime\":1182334278000,\"action\":\"show_post-topic\",\"document_id\":\"bbs_thread|409985\",\"content\":\"\",\"account\":\"10002-4\"}")
//
//    HBaseUtil.put(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|7778",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId)),
//      F,"65472000","{\"atime\":1182334278000,\"action\":\"list_disease-post\",\"document_id\":\"bbs_thread|7778\",\"content\":\"\",\"account\":\"10002-4\"}")
//
//    HBaseUtil.put(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|7777",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId)),
//      F,"7777","{\"atime\":1182334278000,\"action\":\"show_post\",\"document_id\":\"bbs_thread|7777\",\"content\":\"\",\"account\":\"bbs\"}")
//  }
//
//  override def afterEach(): Unit = {
//    HBaseUtil.delete(tableNames(0), HBaseContent.piUserInfoRowkey(userId, HBaseContent.TAG_TYPE_U))
//    HBaseUtil.delete(tableNames(1), HBaseContent.piUserLogRowkey(HBaseContent.TAG_TYPE_U, HBaseContent.stringToMD5(userId), day))
//
//    HBaseUtil.delete(HBaseContent.piUserContentTableName, HBaseContent.piUserContentRowkey(userId,day))
//    HBaseUtil.delete(HBaseContent.piUserContentTableName, HBaseContent.piUserContentRowkey(userId,day))
//
//    HBaseUtil.delete(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|25981720",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId))
//    )
//
//    HBaseUtil.delete(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|409985",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId))
//    )
//
//    HBaseUtil.delete(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|409986",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId))
//    )
//
//    HBaseUtil.delete(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|7777",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId))
//    )
//
//    HBaseUtil.delete(HBaseContent.piDocumentUserActionTableName,
//      HBaseContent.piDocumentUserActionRowkey("bbs_thread|7778",day,HBaseContent.TAG_TYPE_U,HBaseContent.stringToMD5(userId))
//    )
//  }
//
//  //可参考 http://www.scalatest.org/user_guide/using_matchers
//
//  "dxyid" should "fetch action list success" in {
//    val actionList = new PiActiveIndexAccumulator().getDoctActionListByUserID(userId, day)
//    actionList should have size 1
//    actionList(0)._2 should startWith ("event_share")
//  }
//
//  "dxyid" should "compute active" in {
//    DoctorIndexDao.updateDoctorIndexToday(userId)
//    val result = HBaseUtil.get(tableNames(2), HBaseContent.piDoctorIndexRowkey(userId, day), "f","bbs_1")
//    result.rawCells().map{f=>
//      val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(f)))
//      (valueJson \ "activity_score").asOpt[Long].getOrElse(0) shouldBe 3
//    }
//    HBaseUtil.delete(HBaseContent.piDoctorIndexTableName, HBaseContent.piDoctorIndexRowkey(userId, day))
//  }
//
//  "dxyid" should "compute influent" in {
//    DoctorIndexDao.updateDoctorInfluenceIndexToday(userId,1360)
//    var result = HBaseUtil.get(HBaseContent.piDoctorIndexTableName, HBaseContent.piDoctorIndexRowkey(userId, day), "f","10002-4")
//    HBaseUtil.showCell(result)
//
//    result.rawCells().map{f=>
//      val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(f)))
//      (valueJson \ "influence_score").asOpt[Long].getOrElse(0) shouldBe 3
//    }
//
//    result = HBaseUtil.get(HBaseContent.piDoctorIndexTableName, HBaseContent.piDoctorIndexRowkey(userId, day), "f","bbs")
//    HBaseUtil.showCell(result)
//
//    result.rawCells().map{f=>
//      val valueJson = Json.parse(HBaseUtil.toStr(CellUtil.cloneValue(f)))
//      (valueJson \ "influence_score").asOpt[Long].getOrElse(0) shouldBe 2
//    }
//
//    HBaseUtil.delete(HBaseContent.piDoctorIndexTableName, HBaseContent.piDoctorIndexRowkey(userId, day))
//  }
//
//}
