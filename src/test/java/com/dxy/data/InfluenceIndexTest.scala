//package com.dxy.data
//
//import java.text.SimpleDateFormat
//import java.util.Date
//
//import com.dxy.data.hbase.daos.{PiDoctorIndexDao, PiUserContentDao}
//import com.dxy.data.util.{ConfigUtil, DoctorIndexDao, PiInfluenceIndexAccumulator}
//import com.dxy.data.app.DoctorInfluenceIndexStreaming
//
//
//class InfluenceIndexTest extends BaseTest{
//  val day = new SimpleDateFormat("yyyy-MM-dd").parse("2017-02-28")
//  val userId = "txgeek"
//
//  "txgeek" should "get all his content" in {
//    val userContents = new PiUserContentDao().findContentsByUserId(userId,10000)
//    userContents.size should be > 1
//  }
//
//  "txgeek' content" should "get all his actions" in {
//
//    val userActions = new PiInfluenceIndexAccumulator().getActionListFromUserDocument(userId,1000,day)
//    println(userActions)
//    userActions.size should be > 1
//  }
//
//  "txgeek" should "compute his influence" in {
//    val influenceService = new PiInfluenceIndexAccumulator()
//    val influenceScore = influenceService.getDoctorInfluenceIndexMapByUserId(userId ,1000,day)
//    println(influenceScore)
//  }
//
//  "txgeek" should "output result before to hbase" in {
//    val influenceService = new PiInfluenceIndexAccumulator()
//    val influenceScore = influenceService.getDoctorInfluenceIndexMapByUserId(userId ,1000,day)
//    // Go to HBase shell check if txgeek's entry correct.
//    new PiDoctorIndexDao().putDoctorIndexDay(userId, day, influenceScore)
//  }
//
//  "txgeek" should "be OK in spark streaming" in {
//    DoctorInfluenceIndexStreaming.main(Array())
//  }
//
//
//}
